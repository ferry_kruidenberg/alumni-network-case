import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App/App';
import reportWebVitals from './reportWebVitals';
import AppLoading from "./App/AppLoading";
import KeycloakService from "./services/KeycloakService";
import {createStore} from "redux";
import allReducers from "./reducers";
import {Provider} from 'react-redux'
import 'jquery/dist/jquery.min.js';
import 'bootstrap/dist/js/bootstrap.min.js';
import 'bootstrap/dist/css/bootstrap.min.css'

const store = createStore(
    allReducers,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

// Temporary render while keycloak initialises.
ReactDOM.render(
    <Provider store={store}>
        <AppLoading />
    </Provider>
    , document.getElementById('root')
    );

KeycloakService.initKeycloak(() => {
    // Render the actual application.
    ReactDOM.render(
        <React.StrictMode>
            <Provider store={store}>
            <App />
            </Provider>
        </React.StrictMode>,
        document.getElementById('root')
    );
})

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
