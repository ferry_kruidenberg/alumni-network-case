const userReducer = (state =
                         {
                             id: '',
                             userName: '',
                             name: '',
                             lastName: '',
                             status: '',
                             bio: '',
                             quote: '',
                             profilePicture: '',
                             groups: []
                         }, action) => {
    switch(action.type){
        case 'SET_USER':
            return action.payload;
        case 'ADD_GROUP_TO_USER':
            const groups = state.groups
            groups.push(action.payload);
            return {...state, groups};
        case 'REMOVE_GROUP_FROM_USER':
            const groups2 = state.groups
            const index = groups2.indexOf(action.payload);
            if(index > -1){
                groups2.splice(index,1);
            }
            return {...state, groups2};
        default:
            return state;
    }
}

export default userReducer;
