function removeItemOnce(arr, value) {
    const index = arr.indexOf(value);
    if (index > -1) {
        arr.splice(index, 1);
    }
    return arr;
}

const messagesReducer = (state =
                             [], action) => {
    switch (action.type) {
        case 'SET_MESSAGES':
            return action.payload;
        case 'ADD_MESSAGES':
            if (action.payload.replyParentId !== null) {
                const index = state.findIndex(message => message.id === action.payload.replyParentId)
                const parentMessage = state[index]
                parentMessage.threadMessages.push(action.payload.id)
                return [
                    ...state.slice(0, index),
                    parentMessage,
                    ...state.slice(index + 1),
                    action.payload
                ]
            }
            return [...state, action.payload]
        case 'REMOVE_MESSAGES':
            if (action.payload.replyParentId != null){
                const index = state.findIndex(message => message.id === action.payload.replyParentId)
                const parentMessage = state[index]
                parentMessage.threadMessages = removeItemOnce(parentMessage.threadMessages, action.payload.id)
                state = [
                    ...state.slice(0, index),
                    parentMessage,
                    ...state.slice(index + 1),]
            }
            return state.filter(message => message!==action.payload)
        case 'EDIT_MESSAGES':
            const index = state.findIndex(message => message.id === action.payload.id)
            const message = state[index]
            message.messageText = action.payload.messageText
            return [
                ...state.slice(0, index),
                message,
                ...state.slice(index + 1)
            ]
        default:
            return state;
    }
}

export default messagesReducer;
