import {combineReducers} from "redux";
import userReducer from "./user";
import usernameByIdReducer from "./usernameById";
import messagesReducer from "./message";
import groupNameByIdReducer from "./groupnameById";

const allReducers = combineReducers({
    user : userReducer,
    usernameById: usernameByIdReducer,
    groupNameById: groupNameByIdReducer,
    messages: messagesReducer
});

export default allReducers;
