const groupNameByIdReducer = (state =
                                 {} , action) => {
    switch(action.type){
        case 'ADD_GROUP':
            const dict = state
            dict[action.payload.id] = action.payload.name;
            return {...state, dict};
        default:
            return state;
    }

}


export default groupNameByIdReducer
