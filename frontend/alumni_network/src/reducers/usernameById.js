const usernameByIdReducer = (state =
                                 {} , action) => {
    switch(action.type){
        case 'ADD_USER':
            const dict = state
            dict[action.payload.id] = action.payload.username;
            return {...state, dict};
        default:
            return state;
    }

}


export default usernameByIdReducer
