export const setUser = (user) => {
    return {
        type: 'SET_USER',
        payload: user
    }
}

export const addGroupToUser = (group) => {
    return {
        type: 'ADD_GROUP_TO_USER',
        payload: group
    }
}

export const removeGroupFromUser = (group) => {
    return {
        type: 'REMOVE_GROUP_FROM_USER',
        payload: group
    }
}

export const addUser = (user) => {
    return {
        type: 'ADD_USER',
        payload: user
    }
}

export const addGroup = (group) => {
    return {
        type: 'ADD_GROUP',
        payload: group
    }
}

export const setMessages = (messages) => {
    return {
        type: 'SET_MESSAGES',
        payload: messages
    }
}

export const addMessages = (message) => {
    return {
        type: 'ADD_MESSAGES',
        payload: message
    }
}

export const removeMessages = (message) => {
    return {
        type: 'REMOVE_MESSAGES',
        payload: message
    }
}

export const editMessages = (message) => {
    return {
        type: 'EDIT_MESSAGES',
        payload: message
    }
}


