import KeycloakService from "../../services/KeycloakService"
import {Redirect} from "react-router-dom"

const Login = () => {
    if (KeycloakService.isLoggedIn()) {
        return <Redirect to="/messagefeed" />
    }

    const handleLoginClick = () => {
        KeycloakService.doLogin()
    }

    return (
        <div>
            <h1>Please Login</h1>
            <button onClick={ handleLoginClick }>Login</button>
        </div>
    )


}
export default Login
