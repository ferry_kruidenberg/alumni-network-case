import {getUserByUsernameAPI} from "../../API/userAPI"
import {useState} from "react"
import ProfileInfoEdit from "../../Components/Profile/ProfileInfoEdit"

const UserProfiles = () => {
    const [username, setUsername] = useState("")
    const [errorMessage, setErrorMessage] = useState("")
    const [user, setUser] = useState(null)


    const findUser = async () => {
        const [error, result] = await getUserByUsernameAPI(username)
        if (error == null) {
            setUser(result)
            setErrorMessage("")
        } else if (error.includes("404")) {
            setErrorMessage("User not found")
            setUser(null)
        }
    }

    return (
        <>
            <h1>
                User Profiles
            </h1>

            <div className="text-center d-flex justify-content-center">
                <div className="w-50">
                    <ul className="list-group list-unstyled ms-3 me-3 bg-white border border-dark">
                        <li className="mt-2">
                            Username:
                            <textarea
                                required
                                id="nameField"
                                className="inputFieldSingleLine border border-dark"
                                maxLength="50"
                                onChange={(event => setUsername(event.target.value))}
                            />
                        </li>
                        <li>
                            <button disabled={username.length === 0} onClick={findUser}
                                    className="btn-secondary rounded-2 px-2 mb-2">Search
                            </button>
                        </li>
                        <li>
                            <p>{errorMessage}</p>
                        </li>
                    </ul>
                </div>
            </div>
            {user != null &&
            <>
                <hr
                    style={{
                        color: "gray",
                        backgroundColor: "gray",
                        height: 2
                    }}
                />
                <ProfileInfoEdit>{user}</ProfileInfoEdit>
            </>
            }
        </>
    )
}
export default UserProfiles
