import WithKeycloak from "../../Components/hoc/WithKeycloak"
import {useEffect, useState} from "react"
import {addUserToGroupAPI, getSingleGroupAPI, updateGroupAPI} from "../../API/groupAPI"
import {Link} from "react-router-dom"
import SubscribeButton from "../../Components/Profile/SubscribeButton"
import GroupMessages from "./GroupMessages"
import CreateMessage from "../../Components/Timeline/CreateMessage"
import {useSelector} from "react-redux"
import CreateEditGroup from "../../Components/Group/CreateEditGroup"
import InviteComponent from "../../Components/Group/InviteComponent"

const GroupPage = () => {
    const user = useSelector(state => state.user)
    const [userHasAccessToPage, setUserHasAccessToPage] = useState(false)
    const groupId = Number(window.location.pathname.toString().split('/')[2])
    const [group, setGroup] = useState({
        id: -1,
        name: '',
        isPrivate: false,
        description: '',
        creatorId: 0,

        users: [],
        messages: []
    })

    const [newMessage, setNewMessage] = useState(false)
    const [editGroup, setEditGroup] = useState(false)
    const [invite, setInvite] = useState(false)

    const parentMessage = {
        id: null,
        targetUserId: null,
        targetGroupId: groupId
    }

    const toggleNewMessage = () => {
        setNewMessage(!newMessage)
    }

    const toggleEditGroup = () => {
        setEditGroup(!editGroup)
    }

    const toggleInvite = () => {
        setInvite(!invite)
    }

    const updateCurrentGroup = async (groupData) => {
        const error = (await updateGroupAPI(groupId, groupData))[0]
        if (error == null) {
            setGroup({...group, ...groupData})
            toggleEditGroup()
        }
    }

    //Add user with userId to the group
    const updateGroupUsers = async (userId) => {
        //backend needs to update
        const error = await addUserToGroupAPI(group.id, userId)
        if (error == null) {
            setGroup({...group, users: [...group.users, userId]})
        }
    }

    useEffect(() => {
        const fetchGroup = async () => {
            const [error, result] = await getSingleGroupAPI(groupId)
            if (error === null) {
                setGroup(result)
                setUserHasAccessToPage(true)
            }
        }
        fetchGroup()
    }, [groupId])


    return (
        <>
            {!userHasAccessToPage &&
            <div>
                <br/>
                <p>This group does not seem to exist or you do not have access to this group</p>
                <Link to={"/groups"}>Return to group page</Link>
            </div>
            }
            {userHasAccessToPage &&
            <div>
                <div className="backButton ms-3">
                    <Link className="p-2 float-start" to={"/groups"} style={{"textDecoration": "none"}}>
                        <button
                            className="btn-danger py-1 px-3 text-white rounded-3 d-flex align-items-center justify-content-center">
                            <span className="material-icons">arrow_back</span>
                            Groups
                        </button>
                    </Link>
                </div>
                <h1>{group.name}</h1>
                <p>{group.description}</p>
                <div>
                    <SubscribeButton key={group}>
                        {group}
                    </SubscribeButton>
                </div>

                {user.groups.includes(group.id) &&
                <div>
                    <button className="btn-secondary rounded-2 mt-3 mx-1" onClick={toggleNewMessage}>New message
                    </button>
                    {group.creatorId === user.id &&
                    <button className="btn-secondary rounded-2 mt-3 mx-1" onClick={toggleEditGroup}>Edit group</button>
                    }
                    {group.isPrivate &&
                    <button className="btn-secondary rounded-2 mt-3 mx-1" onClick={toggleInvite}>Invite</button>
                    }

                    <ul className="list-unstyled me-3 mt-3 mb-0">
                        {newMessage &&
                        <li>
                            <CreateMessage onClick={toggleNewMessage} parent={parentMessage}/>
                        </li>
                        }
                        {editGroup &&
                        <li>
                            <CreateEditGroup submit={updateCurrentGroup} cancel={toggleEditGroup} group={group}/>
                        </li>
                        }
                        {invite &&
                        <li>
                            <InviteComponent submit={updateGroupUsers} close={toggleInvite} parent={parentMessage}/>
                        </li>
                        }
                    </ul>

                </div>
                }

                <div className="pb-4 pt-4">
                    <GroupMessages key={group.id}>
                        {group.id}
                    </GroupMessages>
                </div>
            </div>
            }
        </>
    )
}
export default WithKeycloak(GroupPage)
