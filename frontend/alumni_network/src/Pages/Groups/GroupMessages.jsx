import WithKeycloak from "../../Components/hoc/WithKeycloak"
import Timeline from "../../Components/Timeline/Timeline"
import {useEffect} from "react"
import {getMessagesFromGroupAPI} from "../../API/groupAPI"
import {setMessages} from "../../actions"
import {useDispatch, useSelector} from "react-redux"

const GroupMessages = (props) => {
    const groupId = props.children
    const messages = useSelector(state => state.messages)
    const dispatch = useDispatch()


    useEffect(() => {
        const getMessage = async () => {
            const [error, result] = await getMessagesFromGroupAPI(groupId)
            if (error == null) {
                dispatch(setMessages(result))
            }
        }
        getMessage()
    }, [groupId])



    return (
        <>
            <Timeline>
                {messages}
            </Timeline>
        </>
    )
}
export default WithKeycloak(GroupMessages)
