import WithKeycloak from "../../Components/hoc/WithKeycloak"
import {postGroupAPI, getAllGroupsAPI} from "../../API/groupAPI"
import {useEffect, useState} from "react"
import {Link} from "react-router-dom"
import SubscribeButton from "../../Components/Profile/SubscribeButton"
import 'bootstrap/dist/css/bootstrap.min.css'
import './Group.css'
import {useDispatch} from "react-redux"
import {addGroupToUser} from "../../actions"
import CreateEditGroup from "../../Components/Group/CreateEditGroup"


const Groups = () => {
    const [groups, setGroups] = useState([])

    const [createMessage, setCreateMessage] = useState(false)
    const dispatch = useDispatch()

    const createGroup = async (groupData) => {
        const [error, result] = await postGroupAPI(groupData)
        if (error == null) {
            setGroups([...groups, result])
            dispatch(addGroupToUser(result.id))
            toggleCreate()
        }
    }


    const toggleCreate = () => {
        setCreateMessage(!createMessage)
    }


    useEffect(() => {
        const fetchGroups = async () => {
            const [error, result] = await getAllGroupsAPI()
            if (error == null) {
                setGroups(result)
            }
        }
        fetchGroups()
    }, [])

    return (
        <>
            <h1 className="pt-3 pb-2">
                Public groups
            </h1>
            <button onClick={toggleCreate} className="btn-secondary rounded-2 px-2 mb-2">New group</button>
            {createMessage &&
            <CreateEditGroup submit={createGroup} cancel={toggleCreate}/>
            }
            <ul className="list-group rounded-3">
                {groups.length !== 0 &&
                groups.map(group => {
                    return (<li className="list-group-item ms-3 me-3 mt-1" key={group.id}>
                        <div className="row">
                            <Link className="col-md-4 mt-3 link-primary ms-0 groupName"
                                  to={`/groups/${group.id}`}>{group.name}</Link>
                            <p className="col-md-4 groupDescription pt-3">{group.description}</p>
                            <div className="col-md-4 subscribeButtonDiv pt-2 pb-2">
                                <SubscribeButton key={group}>
                                    {group}
                                </SubscribeButton>
                            </div>
                        </div>
                    </li>)
                })}
            </ul>
        </>
    )
}
export default WithKeycloak(Groups)
