import "./Profile.css"
import WithKeycloak from "../../Components/hoc/WithKeycloak"
import {useSelector} from "react-redux"
import {GetAllGroupsFromUserAPI} from "../../API/userAPI"
import {useEffect, useState} from "react"
import {Link} from "react-router-dom"
import ProfileInfoEdit from "../../Components/Profile/ProfileInfoEdit"

const Profile = () => {
    const user = useSelector(state => state.user)
    const [groups, setGroups] = useState([])

    useEffect(() => {
        const getGroups = async () => {
            const [error, result] = await GetAllGroupsFromUserAPI(user.id)
            if (error === null) {
                setGroups(result)
            }
        }

        if (user.id !== "") {
            getGroups()
        }
    }, [user])


    return (
        <div>
            <h1>Profile</h1>
            <ProfileInfoEdit>{user}</ProfileInfoEdit>
            <hr
                style={{
                    color: "gray",
                    backgroundColor: "gray",
                    height: 2
                }}
            />
            <div>
                <div style={{"width": "min(80%,700px)", "display": "inline-block"}} className="flex-grow-0">
                    <h3>Your groups</h3>
                    <ul className="list-group mt-4  list-unstyled justify-content-center">
                        {groups.length !== 0 &&
                        groups.map(group => {
                            return (<li className="ms-3 me-3 mt-2  rounded-3" key={group.id}>
                                <Link className="list-group-item list-group-item-secondary groupItemObj" to={`/groups/${group.id}`}>{group.name}</Link>
                            </li>)
                        })}
                    </ul>
                </div>
            </div>
        </div>
    )
}
export default WithKeycloak(Profile)
