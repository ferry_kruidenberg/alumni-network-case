import WithKeycloak from "../../Components/hoc/WithKeycloak"
import Timeline from "../../Components/Timeline/Timeline"

import {useDispatch, useSelector} from "react-redux"
import {getMessagesFromUserAPI} from "../../API/userAPI"
import {useEffect} from "react"

import "./MessageFeed.css"
import {setMessages} from "../../actions"

const MessageFeed = () => {
    const user = useSelector(state => state.user)
    const messages = useSelector(state => state.messages)
    const dispatch = useDispatch()

    useEffect(() => {
        const getMessage= async () => {
            const [error, result] = await getMessagesFromUserAPI(user.id)
            if (error == null) {
                dispatch(setMessages(result))
            }
        }

        if (user.id !== "") {
            getMessage()
        }

    }, [user])


    return (
        <>
            <div id="curtain">
                <h1 className="mb-3">Message feed</h1>
            </div>

            <div className="pb-4">
                <Timeline>
                    {messages}
                </Timeline>
            </div>


        </>
    )
}
export default WithKeycloak(MessageFeed)
