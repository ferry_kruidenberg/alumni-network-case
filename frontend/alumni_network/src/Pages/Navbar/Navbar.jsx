import "./Navbar.css"
import React, {useEffect, useRef} from "react"
import KeycloakService from "../../services/KeycloakService"
import {Link} from "react-router-dom"
import {useSelector} from "react-redux"

export const Navbar = (props) => {

    const user = useSelector(state => state.user)
    const sidebar = useRef(null)

    const toggleSidebar = () => {
        sidebar.current.classList.toggle("open")
    }

    const logout = () => {
        KeycloakService.doLogout()
    }

    useEffect(() =>{
        toggleSidebar()
    }, [])

    return (
        <>
            <div className="sidebar" ref={node => sidebar.current = node}>
                <div className="logo-details">
                    <span className="material-icons icon">account_tree</span>
                    <div className="logo_name">Alumni Network</div>
                    <span className="material-icons" id="btn" onClick={toggleSidebar}>menu</span>
                </div>
                <ul className="nav-list">
                    <li>
                        <Link to="/messagefeed">
                            <span className="material-icons icon">feed</span>
                            <span className="links_name">Message feed</span>
                        </Link>
                        <span className="tooltip">Message feed</span>
                    </li>
                    <li>
                        <Link to="/groups">
                            <span className="material-icons icon">groups</span>
                            <span className="links_name">Groups</span>
                        </Link>
                        <span className="tooltip">Groups</span>
                    </li>
                    <li>
                        <Link to="/profile">
                            <span className="material-icons icon">account_circle</span>
                            <span className="links_name">Profile</span>
                        </Link>
                        <span className="tooltip">Profile</span>
                    </li>
                    <li>
                        <Link to="/users">
                            <span className="material-icons icon">person_search</span>
                            <span className="links_name">Find users</span>
                        </Link>
                        <span className="tooltip">Find Users</span>
                    </li>
                    {KeycloakService.isLoggedIn() &&
                    <li className="profile">
                        <div className="profile-details">
                            {/*<img src="profile.jpg" alt="profileImg"/>*/}
                            <div className="name_job">
                                <div className="name">{user.userName}</div>
                                <div className="status">{user.status}</div>
                            </div>
                        </div>
                        <span className="material-icons" id="log_out" onClick={logout}>logout</span>
                    </li>
                    }
                </ul>
            </div>
            <section className="home-section pt-2">
                {props.children}
            </section>
        </>
    )
}
