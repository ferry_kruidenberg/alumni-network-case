import './App.css'
import {BrowserRouter, Redirect, Route, Switch} from "react-router-dom"
import Login from "../Pages/Login/Login"
import {Navbar} from "../Pages/Navbar/Navbar"
import MessageFeed from "../Pages/MessageFeed/MessageFeed"
import KeycloakService from "../services/KeycloakService"
import Profile from "../Pages/Profile/Profile"
import Groups from "../Pages/Groups/Groups"
import {useDispatch} from "react-redux"
import {getUserSelfAPI, postUserAPI} from "../API/userAPI"
import {setUser} from "../actions"
import {useEffect} from "react"
import GroupPage from "../Pages/Groups/GroupPage"
import UserProfiles from "../Pages/UserProfiles/UserProfiles"

function App() {
    const dispatch = useDispatch()

    const handleLogin = async () => {
        const [error, result] = await getUserSelfAPI()
        if (error == null){
            dispatch(setUser(result))
        } else if (error.includes("404")) {
            const [error, result] = await postUserAPI()
            if (error == null) {
                dispatch(setUser(result))
            }
        }
    }


    useEffect(()=>{
        handleLogin()
    }, [handleLogin, KeycloakService.isLoggedIn()])

    return (
        <BrowserRouter>
            <div className="App">
                <Navbar>
                    <Switch>
                        <Route exact path="/">
                            <Redirect to="/login"/>
                        </Route>
                        <Route path="/login" component={Login}/>
                        <Route path="/messagefeed" component={MessageFeed}/>
                        <Route path="/profile" component={Profile}/>
                        <Route exact path="/groups" component={Groups}/>
                        <Route path="/groups/:id" component={GroupPage}/>
                        <Route path="/users" component={UserProfiles}/>
                    </Switch>
                </Navbar>
            </div>
        </BrowserRouter>
    )
}

export default App
