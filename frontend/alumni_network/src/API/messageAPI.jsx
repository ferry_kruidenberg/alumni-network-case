import {fetchDelete, fetchGet, fetchPost, fetchPut} from "./fetchFunctions"

const apiURL = "https://alumni-network-fm.azurewebsites.net/api/v1/message"


export const getSingleMessageAPI = async (id) => {
    return await fetchGet(`${apiURL}/${id}`)
}

export const getThreadMessagesAPI = async (id) => {
    return await fetchGet(`${apiURL}/${id}/threadmessage`)
}

export const postMessageAPI = async ({userId, messageText, replyParentId = null,
                                       targetUserId = null, targetGroupId = null} = {}) => {
    if (targetGroupId == null && targetUserId == null){
        return ["Both targetGroupId and targetUserId are null", null]
    }
    const message = {
        PostUserId: userId,
        MessageText: messageText,
        ReplyParentId: replyParentId,
        TargetUserId: targetUserId,
        TargetGroupId: targetGroupId
    }
    return await fetchPost(`${apiURL}`, JSON.stringify(message))
}

export const putMessageAPI = async (id, messageText) => {
    const body = {
        Id: id,
        MessageText: messageText
    }
    return await fetchPut(`${apiURL}/${id}`, JSON.stringify(body))
}

export const deleteMessageAPI = async (id) => {
    return await fetchDelete(`${apiURL}/${id}`)
}
