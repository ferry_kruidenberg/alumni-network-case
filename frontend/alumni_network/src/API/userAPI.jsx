import {fetchGet, fetchPost, fetchPut} from "./fetchFunctions"
import KeycloakService from "../services/KeycloakService"
import jwt from 'jwt-decode'
const apiURL = "https://alumni-network-fm.azurewebsites.net/api/v1/user"


export const getUserSelfAPI = async () => {
    return await fetchGet(`${apiURL}/self`)
}

export const getUserAPI = async (id) => {
    return await fetchGet(`${apiURL}/${id}`)
}

export const getUserByUsernameAPI = async (username) => {
    return await fetchGet(`${apiURL}/username/${username}`)
}

export const getUsersAPI = async () => {
    return await fetchGet(`${apiURL}`)
}

export const getGroupsFromUserAPI = async (id) => {
    return await fetchGet(`${apiURL}/${id}/groups`)
}

export const getMessagesFromUserAPI = async (id) => {
    return await fetchGet(`${apiURL}/${id}/messages`)
}

export const postUserAPI = async () => {
    const token = jwt(KeycloakService.getToken())
    const body = {
        Name: token.given_name,
        LastName: token.family_name,
        Bio: "",
        Quote: "",
        Status: "",
        ProfilePicture: ""
    }
    return await fetchPost(`${apiURL}`, JSON.stringify(body))
}

export const putUserAPI = async (user) => {
    const body = {
        Id: user.id,
        Name: user.name,
        LastName: user.lastName,
        Bio: user.bio,
        Quote: user.quote,
        Status: user.status,
        ProfilePicture: user.profilePicture
    }
    return await fetchPut(`${apiURL}/${user.id}`, JSON.stringify(body))
}

export const GetAllGroupsFromUserAPI = async (id) => {
    return await fetchGet(`${apiURL}/${id}/groups`)
}




