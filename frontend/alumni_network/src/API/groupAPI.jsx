import {fetchGet, fetchPost, fetchPut} from "./fetchFunctions"

const apiURL = "https://alumni-network-fm.azurewebsites.net/api/v1/group"

export const getAllGroupsAPI = async () => {
    return await fetchGet(`${apiURL}`)
}

export const getSingleGroupAPI = async (id) => {
    return await fetchGet(`${apiURL}/${id}`)
}

export const postGroupAPI = async (group) => {
    const body = {
        IsPrivate: group.isPrivate,
        Name: group.name,
        Description: group.description
    }
    return await fetchPost(`${apiURL}`, JSON.stringify(body))
}

export const updateGroupAPI = async (id, group) => {
    const body = {
        Id: id,
        IsPrivate: group.isPrivate,
        Name: group.name,
        Description: group.description
    }
    return await fetchPut(`${apiURL}/${id}`, JSON.stringify(body))
}

export const addUserToGroupAPI = async (groupId, userId) => {
    return await fetchPut(`${apiURL}/${groupId}/add/${userId}`, JSON.stringify({}))
}

export const removeUserFromGroupAPI = async (groupId, userId) => {
    return await fetchPut(`${apiURL}/${groupId}/remove/${userId}`, JSON.stringify({}))
}

export const getUsersFromGroupAPI = async (id) => {
    return await fetchGet(`${apiURL}/${id}/users`)
}

export const getMessagesFromGroupAPI = async (id) => {
    return await fetchGet(`${apiURL}/${id}/messages`)
}


