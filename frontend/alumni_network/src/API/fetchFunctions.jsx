import KeycloakService from "../services/KeycloakService"

export const fetchGet = async (URL, retry = true) => {
    try {
        const results = await fetch(URL, {
            method: 'GET',
            headers: {
                'Authorization': "Bearer " + KeycloakService.getToken(),
                'Content-Type': 'application/json'
            },
        }).then(async response => {
            if (!response.ok) {
                if (retry && response.status === 401) {
                    await KeycloakService.updateToken(() => {
                    })
                    return fetchPut(URL, false)
                } else {
                    throw new Error(`${response.status}: ${response.statusText}`)
                }
            } else {
                return response.json()
            }
        })
        return [null, results]
    } catch (e) {
        return [e.message, null]
    }
}

export const fetchPost = async (URL, body, retry = true) => {
    try {
        const results = await fetch(URL, {
            method: 'POST',
            headers: {
                'Authorization': "Bearer " + KeycloakService.getToken(),
                'Content-Type': 'application/json'
            },
            body: body
        }).then(async response => {
            if (!response.ok) {
                if (retry && response.status === 401) {
                    await KeycloakService.updateToken(() => {
                    })
                    return fetchPut(URL, body, false)
                }
                throw new Error(`${response.status}: ${response.statusText}`)
            } else {
                return response.json()
            }
        })
        return [null, results]
    } catch (e) {
        return [e.message, null]
    }
}

export const fetchPut = async (URL, body, retry = true) => {
    try {
        const results = await fetch(URL, {
            method: 'PUT',
            headers: {
                'Authorization': "Bearer " + KeycloakService.getToken(),
                'Content-Type': 'application/json'
            },
            body: body
        }).then(async response => {
            if (!response.ok) {
                if (retry && response.status === 401) {
                    await KeycloakService.updateToken(() => {
                    })
                    return fetchPut(URL, body, false)
                }
                throw new Error(`${response.status}: ${response.statusText}`)
            } else {
                return response
            }
        })
        return [null, results]
    } catch (e) {
        return [e.message, null]
    }
}

export const fetchDelete = async (URL, retry = true) => {
    try {
        const results = await fetch(URL, {
            method: 'DELETE',
            headers: {
                'Authorization': "Bearer " + KeycloakService.getToken(),
                'Content-Type': 'application/json'
            },
        }).then(async response => {
            if (!response.ok) {
                if (retry && response.status === 401) {
                    await KeycloakService.updateToken(() => {
                    })
                    return fetchPut(URL, false)
                }
                throw new Error(`${response.status}: ${response.statusText}`)
            } else {
                return response
            }
        })
        return [null, results]
    } catch (e) {
        return [e.message, null]
    }
}
