import gfm from "remark-gfm"
import rehypeRaw from "rehype-raw"
import ReactMarkdown from "react-markdown"
import {useDispatch, useSelector} from "react-redux"
import './MessageBody.css'
import {deleteMessageAPI} from "../../API/messageAPI"
import {confirmAlert} from 'react-confirm-alert' // Import
import 'react-confirm-alert/src/react-confirm-alert.css'
import {removeMessages} from "../../actions"
import {useEffect, useState} from "react"
import CreateMessage from "./CreateMessage"
import {getSingleGroupAPI} from "../../API/groupAPI";
import {Link} from "react-router-dom";

const MessageBody = (props) => {
    const message = props.children.message
    const usernameById = useSelector(state => state.usernameById)
    const groupNameById = useSelector(state => state.groupNameById)
    const user = useSelector(state => state.user)
    const dispatch = useDispatch()
    const [edit, setEdit] = useState(false)
    const [groupName, setGroupName] = useState("")

    const convertTime = (time) => {
        return time.split(".")[0].replace("T", " ")
    }

    const deleteMsg = () => {
        confirmAlert({
            title: 'Delete message',
            message: 'Are you sure to do this.',
            buttons: [
                {
                    label: 'Yes',
                    onClick: async () => {
                        const error = (await deleteMessageAPI(message.id))[0]
                        if (error == null) {
                            dispatch(removeMessages(message))
                        }
                    }
                },
                {
                    label: 'No'
                }
            ]
        })
    }

    const toggleEditMsg = () => {
        setEdit(!edit)
    }

    useEffect(() => {
        if (window.location.pathname.toString().includes("messagefeed")) {
            setGroupName(groupNameById[message.targetGroupId])
        }
    })

    return (
        <>
            <div className="d-flex justify-content-between">
                <div>
                    {groupName && props.children.mainMessage &&
                    <Link to={`/groups/${message.targetGroupId}`}>{`[${groupName}]`} </Link>
                    }
                    <span
                        className="d-inline-block align-middle">{usernameById[message.postUserId]}: {convertTime(message.timePosted)}</span>
                </div>
                {(props.children.mainMessage || message.postUserId === user.id) &&
                <div className="register-btn">
                    {props.children.mainMessage &&
                    <button onClick={() => props.onClick(message.id)} type="button"
                            className="bg-transparent">
                        <span className="material-icons align-bottom">reply</span>
                    </button>
                    }

                    {message.postUserId === user.id &&
                    <div className="dropdown" style={{float: "right"}}>
                        <button className="bg-transparent">
                            <span className="material-icons align-bottom">menu</span>
                        </button>

                        <div className={`dropdown-content`}>
                            <button onClick={toggleEditMsg} className="dropdown-item">Edit</button>
                            <button onClick={deleteMsg} className="dropdown-item">Delete</button>
                        </div>
                    </div>}

                </div>
                }


            </div>

            <ReactMarkdown id="preview" children={message.messageText} remarkPlugins={[gfm]}
                           rehypePlugins={[rehypeRaw]}/>
            {edit &&
            <CreateMessage edit={true} onClick={toggleEditMsg}>{message}</CreateMessage>}
        </>
    )
}
export default MessageBody
