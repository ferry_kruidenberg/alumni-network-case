import MessageBody from "./MessageBody"

const messageThread = (props) => {
    const messages = props.children.messages
    const threadMessages = props.children.threadMessages

    return (
        <ul className="list-group">
            {messages.filter(message => threadMessages
                .includes(message.id))
                .reverse()
                .map(message => {
                    return (
                        <li className="list-group-item" key={message.id}>
                            <MessageBody>
                                {{message: message,
                                    mainMessage: false,
                                    replyShown: false}}
                            </MessageBody>
                        </li>
                    )
                })}
        </ul>
    )
}
export default messageThread
