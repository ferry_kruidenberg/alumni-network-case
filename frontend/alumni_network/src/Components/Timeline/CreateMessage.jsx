import ReactMarkdown from "react-markdown"
import gfm from "remark-gfm"
import rehypeRaw from "rehype-raw"
import {useEffect, useState} from "react"
import {postMessageAPI, putMessageAPI} from "../../API/messageAPI"
import {useDispatch, useSelector} from "react-redux"
import {addMessages, editMessages} from "../../actions"

const CreateMessage = (props) => {
    const user = useSelector(state => state.user)
    const [preview, setPreview] = useState("")
    const dispatch = useDispatch()

    const updatePreview = (event) => {
        setPreview(event.target.value)
    }

    const sendMessage = async () => {
        const [error, result] = await postMessageAPI({
            userId: user.id,
            messageText: preview,
            replyParentId: props.parent === undefined ? null : props.parent.id,
            targetUserId: props.parent === undefined ? null : props.parent.targetUserId,
            targetGroupId: props.parent === undefined ? null : props.parent.targetGroupId,
        })
        if (error == null) {
            dispatch(addMessages(result))
            props.onClick(props.parent.id)
        } else {
            console.log(error)
        }
    }

    const editMsg = async () => {
        const error = (await putMessageAPI(props.children.id, preview))[0]
        if (error == null) {
            dispatch(editMessages({id: props.children.id, messageText: preview}))
            props.onClick(props.parent?.id)
        } else {
            console.log(error)
        }
    }

    useEffect(() => {
        if (props.edit === true) {
            setPreview(props.children.messageText)
        }

    }, [])

    return (
        <ul className="list-group ms-3">
            <li id="list" className="list-group-item border border-dark rounded-2 pb-3">
                <div className="input-group">
                    <div className="input-group-prepend">

                        {props.edit !== true &&
                        <>
                            <span className="input-group-text mb-3">New message</span>
                            <button disabled={!preview.length} className="btn btn-secondary w-100 mt-2" type="button"
                                    onClick={sendMessage}>Send
                            </button>
                        </>
                        }
                        {props.edit === true &&
                        <>
                            <span className="input-group-text mb-3">Edit message</span>
                            <button disabled={!preview.length} className="btn btn-secondary w-100 mt-2" type="button"
                                    onClick={editMsg}>Send
                            </button>
                        </>
                        }

                        <button className="btn btn-secondary w-100 mt-2" type="button"
                                onClick={() => props.onClick(props.parent?.id)}>Cancel
                        </button>
                    </div>
                    <textarea id="textarea" onChange={updatePreview} className="form-control border border-dark ms-1"
                              aria-label="With textarea" defaultValue={props.edit && props.children.messageText}/>
                    <div id="preview" className="ms-1 border border-dark">
                        <ReactMarkdown id="preview" children={preview} remarkPlugins={[gfm]}
                                       rehypePlugins={[rehypeRaw]}/>
                    </div>
                </div>
            </li>
        </ul>
    )
}
export default CreateMessage
