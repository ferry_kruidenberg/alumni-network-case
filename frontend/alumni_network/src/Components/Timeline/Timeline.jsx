import MessageBody from "./MessageBody"
import MessageThread from "./MessageThread"
import CreateMessage from "./CreateMessage"
import {useEffect, useState} from "react"
import {useDispatch} from "react-redux"
import {getUserAPI} from "../../API/userAPI"
import {addGroup, addUser} from "../../actions"
import {getSingleGroupAPI} from "../../API/groupAPI";

const Timeline = (props) => {
    const messages = props.children
    const headMessages = messages.filter(message => message?.replyParentId == null).reverse()
    const [showReplyMessage, updateShowReplyMessage] = useState([])
    const [showReplyThread, updateShowReplyThread] = useState([])
    const dispatch = useDispatch()

    const onChildClicked = (messageId) => {
        if (!showReplyMessage.includes(messageId)) {
            updateShowReplyMessage(arr => [...arr, messageId])
        } else {
            updateShowReplyMessage(showReplyMessage.filter(e => e !== messageId))
        }
    }

    const toggleReplyThread = (messageId) => {
        if (!showReplyThread.includes(messageId)) {
            updateShowReplyThread(arr => [...arr, messageId])
        } else {
            updateShowReplyThread(showReplyThread.filter(e => e !== messageId))
        }
    }

    //Caching of the usernames
    const setUsernames = async (result) => {
        const users = []
        for (const message of result) {
            if (!users.includes(message.postUserId)) {
                users.push(message.postUserId)
                const [error, result] = await getUserAPI(message.postUserId)
                if (error == null) {
                    dispatch(addUser({
                        id: message.postUserId,
                        username: result.userName
                    }))
                }
            }
        }
    }


    //Caching of the groupNames
    const setGroupNames = async (result) => {
        const groups = []
        for (const message of result) {
            if (!groups.includes(message.targetGroupId)) {
                groups.push(message.targetGroupId)
                const [error, result] = await getSingleGroupAPI(message.targetGroupId)
                if (error == null) {
                    dispatch(addGroup({
                        id: message.targetGroupId,
                        name: result.name
                    }))
                }
            }
        }
    }

    useEffect(() => {
        const getNames = async () => {
            await setUsernames(props.children)
            await setGroupNames(props.children)
        }
        getNames()
    })

    return (
        <>
            <ul className="list-group ms-3 me-3 text-start">
                {headMessages.map(headMessage => {
                    return (
                        <li className="list-group-item border border-dark" key={headMessage.id}>
                            <MessageBody onClick={onChildClicked}>
                                {{
                                    message: headMessage,
                                    mainMessage: true,
                                    replyShown: showReplyMessage.includes(headMessage.id)
                                }}
                            </MessageBody>
                            {showReplyMessage.includes(headMessage.id) &&
                            <CreateMessage onClick={onChildClicked} parent={headMessage}/>}
                            {headMessage.threadMessages.length > 0 &&
                            <div className="ms-3">
                                <button className="btn btn-outline-primary btn-sm mb-2 mt-2"
                                        onClick={() => toggleReplyThread(headMessage.id)}
                                        style={{"minWidth": 125}}>
                                    {showReplyThread.includes(headMessage.id) ? "Hide thread" : "Show thread"}
                                </button>
                                {showReplyThread.includes(headMessage.id) &&
                                <MessageThread>
                                    {{
                                        messages: messages,
                                        threadMessages: headMessage.threadMessages
                                    }}
                                </MessageThread>
                                }
                            </div>
                            }

                        </li>
                    )
                })}
            </ul>
        </>
    )
}

export default Timeline
