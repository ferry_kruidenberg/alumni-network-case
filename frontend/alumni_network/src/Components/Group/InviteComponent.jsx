import {useState} from "react"
import {getUserByUsernameAPI} from "../../API/userAPI"

const InviteComponent = (props) => {
    const [inviteButtonActive, setInviteButtonActive] = useState(false)
    const [username, setUsername] = useState("")
    const [errorMessage, setErrorMessage] = useState("")

    const validate = (event) => {
        setUsername(event.target.value)
        if (event.target.value.length === 0){
            setInviteButtonActive(false)
        } else {
            setInviteButtonActive(true)
        }
    }

    const inviteUser = async () => {
        const [error, result] = await getUserByUsernameAPI(username)
        if (error == null){
            setErrorMessage("User invited")
            props.submit(result.id)
        } else if (error.includes("404")){
            setErrorMessage("User not found")
        }
    }


    return (
        <div className="text-center d-flex justify-content-center">
            <div className="w-50">
                <ul className="list-group list-unstyled ms-3 me-3 bg-white border border-dark">
                    <li>
                        Username:
                        <textarea
                            required
                            id="nameField"
                            className="inputFieldSingleLine border border-dark"
                            maxLength="50"
                            onChange={validate}
                        />
                    </li>
                    <li>
                        <button onClick={inviteUser} disabled={!inviteButtonActive} className="btn-secondary rounded-2 px-2 mb-2">Invite</button>
                        <button onClick={props.close} disabled={!inviteButtonActive} className="btn-secondary rounded-2 px-2 mb-2">Close</button>
                    </li>
                    <li>
                        <p>{errorMessage}</p>
                    </li>
                </ul>
            </div>
        </div>
    )
}
export default InviteComponent
