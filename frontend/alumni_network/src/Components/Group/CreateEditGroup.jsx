import {useEffect, useState} from "react"

const CreateEditGroup = (props) => {

    const [createButtonActive, setCreateButtonActive] = useState(false)

    const groupData = props.group === undefined?
        {
        name: "",
        description: "",
        isPrivate: false
        }:
        {
            name: props.group.name,
            description: props.group.description,
            isPrivate: props.group.isPrivate
        }



    const validateGroup = () => {
        const errors = {}

        groupData.name = document.getElementById("nameField").value
        groupData.description = document.getElementById("descriptionField").value
        groupData.isPrivate = document.getElementById("checkBox").checked

        if (!groupData.name) {
            errors.name = "Please enter a name"
        }

        if (!groupData.description) {
            errors.description = "Please enter a description"
        }

        if (Object.keys(errors).length === 0) {
            setCreateButtonActive(true)
        } else {
            setCreateButtonActive(false)
        }
    }

    useEffect(() => {
        validateGroup()
    },[])

    return (
        <div className="text-center d-flex justify-content-center">
            <div className="w-50">
                <ul className="list-group list-unstyled ms-3 me-3 bg-white border border-dark">
                    <li>
                        Name:
                        <br/>

                        <textarea
                            required
                            id="nameField"
                            className="inputFieldSingleLine border border-dark"
                            maxLength="50"
                            defaultValue={groupData.name}
                            onChange={validateGroup}
                        />

                    </li>
                    <li>
                        Private:
                        <input onChange={validateGroup} className="form-check-input ms-2" type="checkbox" defaultChecked={groupData.isPrivate}
                               id="checkBox"/>
                    </li>
                    <li>
                        Description:
                        <br/>

                        <textarea
                            required
                            id="descriptionField"
                            className="form-inline border border-dark"
                            maxLength="500"
                            defaultValue={groupData.description}
                            onChange={validateGroup}/>

                    </li>
                    <li>
                        <button className="btn-secondary rounded-2 px-2 mb-2"
                                disabled={!createButtonActive} onClick={() => props.submit(groupData)}>
                            {props.group === undefined? "Create": "Edit"}
                        </button>
                        <button className="btn-secondary rounded-2 px-2 ms-3 mb-2" onClick={props.cancel}>Cancel
                        </button>
                    </li>
                </ul>
            </div>
        </div>
    )

}
export default CreateEditGroup
