import React, {useEffect, useState} from 'react'
import Resizer from "react-image-file-resizer"

const UploadImage = (props) => {

    const [image, setImage] = useState("https://getstamped.co.uk/wp-content/uploads/WebsiteAssets/Placeholder.jpg")

    function resizeImage(event){
        let fileInput = false
        if (event.target.files[0]) {
            fileInput = true
        }
        if (fileInput) {
            try {
                Resizer.imageFileResizer(
                    event.target.files[0],
                    200,
                    200,
                    "JPEG",
                    100,
                    0,
                    (uri) => {
                        props.setImage(uri)
                    },
                    "base64",
                    100,
                    100
                )
            } catch (err) {
                console.log(err)
            }
        }
    }

    useEffect(()=>{
        setImage(props.children.image)
    },[props])

    return(
        <input style={{"width": "110px"}}
            type="file"
            accept='.png, .jpg'
            onChange={resizeImage}
        />
    )
}
export default UploadImage
