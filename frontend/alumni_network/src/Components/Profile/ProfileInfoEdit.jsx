import {putUserAPI} from "../../API/userAPI"
import {setUser} from "../../actions"
import {useEffect, useState} from "react"
import {useDispatch, useSelector} from "react-redux"
import UploadImage from "./UploadImage"

const ProfileInfoEdit = (props) => {
    const user = props.children
    const self = useSelector(state => state.user)
    const defaultImage = "https://deconova.eu/wp-content/uploads/2016/02/default-placeholder.png"
    const [editing, setEditing] = useState(false)
    const [image, setImage] = useState("")
    const [savebuttonActive, setSavebuttonActive] = useState(true)
    let tempImage = ""


    const userData = {
        ...user
    }

    const dispatch = useDispatch()

    const validateUser = () => {
        const errors = {}

        userData.name = document.getElementById("firstnameField").value
        userData.lastName = document.getElementById("lastNameField").value
        userData.status = document.getElementById("statusField").value
        userData.bio = document.getElementById("bioField").value
        userData.quote = document.getElementById("quoteField").value

        if (!userData.name) {
            errors.name = "Please enter a name"
        } else if (userData.name.length > 50) {
            errors.name = "Name to long."
        }

        if (!userData.lastName) {
            errors.lastName = "Please enter a last name"
        } else if (userData.lastName.length > 50) {
            errors.lastName = "Lastname to long."
        }

        if (userData.status.length > 100) {
            errors.status = "Status message to long."
        }

        if (userData.bio.length > 500) {
            errors.bio = "Bio to long."
        }

        if (userData.quote.length > 100) {
            errors.quote = "Quote to long."
        }

        if (Object.keys(errors).length === 0) {
            setSavebuttonActive(true)
        } else {
            setSavebuttonActive(false)
        }
    }

    const editButton = () => {
        setEditing(!editing)
        setSavebuttonActive(true)
    }

    const setImageFromChild = (newImage) => {
        tempImage = newImage
        if (tempImage !== image) {
            setImage(newImage)
        }
    }

    const childData = (image) => {
        setImageFromChild(image)
        return (
            <UploadImage setImage={setImageFromChild}>
                {
                    {
                        image: image
                    }
                }
            </UploadImage>
        )
    }

    //Cancels the edit and
    const cancelButton = () => {
        if(userData.profilePicture === null){
            setImage(defaultImage)
        } else {
            setImage(userData.profilePicture)
        }
        setEditing(!editing)
    }

    //Saves the updated profile to the database
    const saveButton = async () => {
        setImage(tempImage)
        if(tempImage === defaultImage) {
            userData.ProfilePicture = null
        } else{
            userData.profilePicture = tempImage
        }
        const error = (await putUserAPI(userData))[0]
        if (error == null) {
            dispatch(setUser(userData))
        }

        setEditing(!editing)
    }

    useEffect(() => {
        const loadImage = () => {
            if (user.profilePicture === null) {
                setImage(defaultImage)
            } else {
                setImage(user.profilePicture)
            }
        }

        if (user.id !== "") {
            loadImage()
        }
    }, [user])

    return (
        <>
            <div className="imageFieldAlign"/>
            <div className="profileBody">
                <div id="UserData">
                    <h4>{user.userName}</h4>
                    {!editing &&
                    <div>
                        <h4>{user.name} {user.lastName}</h4>
                        <h4>{user.status}</h4>
                        <h4>{user.bio}</h4>
                        <h4>{user.quote}</h4>
                    </div>
                    }
                    {editing &&
                    <ul className="list-group list-unstyled">
                        <li>
                            Name:<br/>
                            <textarea
                                required
                                id="firstnameField"
                                maxLength="50"
                                className="inputFieldSingleLine"
                                defaultValue={userData.name}
                                onChange={validateUser}
                            />
                            <textarea
                                required
                                id="lastNameField"
                                maxLength="50"
                                className="inputFieldSingleLine"
                                defaultValue={userData.lastName}
                                onChange={validateUser}/>
                        </li>
                        <li>
                            Status:<br/>
                            <textarea
                                id="statusField"
                                className="inputFieldMultiLine"
                                maxLength="100"
                                defaultValue={userData.status}
                                onChange={validateUser}/>
                        </li>
                        <li>
                            Bio:<br/>
                            <textarea
                                id="bioField"
                                className="inputFieldMultiLine"
                                maxLength="500"
                                defaultValue={userData.bio}
                                onChange={validateUser}/>
                        </li>
                        <li>
                            Quote:<br/>
                            <textarea
                                id="quoteField"
                                className="inputFieldMultiLine"
                                maxLength="100"
                                defaultValue={userData.quote}
                                onChange={validateUser}/>
                        </li>
                    </ul>}
                </div>

                {!editing &&
                user.id === self.id &&
                <button className="m-1 px-2 py-1 btn-outline-secondary" onClick={editButton}>Edit</button>
                }

                {editing &&
                <div>
                    <button className="m-1 px-2 py-1 btn-outline-secondary" onClick={cancelButton}>Cancel</button>
                    <button className="m-1 px-2 py-1 btn-outline-secondary" disabled={!savebuttonActive}
                            onClick={saveButton}>Save changes
                    </button>
                </div>
                }
            </div>
            <div id="Image" className="imageFieldAlign align-top">
                <img className="profilePicture" src={image}/>
                <div className="buttonFieldAlign mt-2">
                    {editing &&
                    childData(image)
                    }
                </div>
            </div>
        </>
    )
}
export default ProfileInfoEdit
