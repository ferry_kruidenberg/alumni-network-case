import {addUserToGroupAPI, removeUserFromGroupAPI} from "../../API/groupAPI"
import {addGroupToUser, removeGroupFromUser} from "../../actions"
import {useDispatch, useSelector} from "react-redux"
import '../../Pages/Groups/Group.css'

const SubscribeButton = (props) => {

    const group = props.children
    const user = useSelector(state => state.user)
    const dispatch = useDispatch()

    const subscribe = async (event) => {
        const error = (await addUserToGroupAPI(event.target.id, user.id))[0]
        if (error == null){
            dispatch(addGroupToUser(Number(event.target.id)))
        }

    }

    const unsubscribe = async (event) => {
        const error = (await removeUserFromGroupAPI(event.target.id, user.id))[0]
        if (error == null){
            dispatch(removeGroupFromUser(Number(event.target.id)))
        }
    }

    return (
        <>
            {user.groups.includes(group.id) &&
                user.id !== group.creatorId &&
            <button className="btn-danger text-white rounded-3 ps-1 pe-1 subscribeButton" id={group.id} onClick={unsubscribe}>Unsubscribe</button>
            }
            {!user.groups.includes(group.id) &&
            <button className="btn-primary text-white rounded-3 ps-1 pe-1 subscribeButton" id={group.id} onClick={subscribe}>Subscribe</button>
            }
            {user.id === group.creatorId &&
            <p className="text-secondary">You are owner of this group</p>}
        </>
    )
}
export default SubscribeButton
