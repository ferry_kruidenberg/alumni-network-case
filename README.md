# Alumni Network Case
### Ferry Kruidenberg & Mark de Wit

The final and big assignment for the Experis Autumn Full Stack 2021 course

[Frontend](#frontend)
- [Login](#login)
- [Profile](#profile)
  - [Edit profile](#edit-profile)
  - [Change profile picture](#change-profile-picture)
  - [Save changes](#save-changes)
- [Find profile](#find-profile)
- [Group List](#group-list)
  - [Create group](#create-group)
  - [Subscribe and Unsubscribe](#subscribe-and-unsubscribe)
  - [Post message](#post-new-message)
- [Message feed](#message-feed)
- [Message](#messages)
  - [Reply](#reply)
  - [Edit](#edit)
  - [Delete](#delete)


[Backend](#backend)
- [Swagger](#swagger)
  - [Group](#group)
  - [Message](#message)
  - [User](#user)
- [ASP .NET Plugins](#asp-net-plugins)

___


# Frontend

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.


## Login

The user can login to the application using Keycloak.

## Profile

Displays the information of the user and their profile picture.

Also contains a list of groups the user is in. Unlike the group list, this also shows private groups.

### Edit profile

The user may edit the information on their profile.

The name and last name must be filled in to update the information.

Fields which are not correctly filled in will be marked in red.

### Change profile picture

The user may upload an image to use as their profile picture. Only *.jpg and *.png are allowed.

When an image is uploaded, the profile will show a preview of the image to the user.

When the uploaded file is not an image,it will not update the profile picture.

### Save changes

After updating their profile information and/or changing their profile picture, the user has the option to save the changes made.

If the user decides to cancel, all changes are discarded.

## Find profile

Input a username to view their profile.

If the username does not exist, it returns "User not found"

## Group list

All public groups are displayed here.

Clicking on a group redirects the user to that group's page.

### Create group

By pressing the ***New Group*** button the user can create a new group. This requires a name and description to be filled in. The user may also choose to make a group private by checking the ***private*** checkbox.


### Subscribe and Unsubscribe

The user may choose to subscribe or unsubscribe to a group. If they are the owner of a group, they cannot unsubscribe. A message explaining this is shown instead of the unsubscribe button.

## Group page

This page displays the information of a group to the user. If the group page is private and the user is not in the group, or if the group page does not exist, the user is given an error message explaining something went wrong. No information is given explaining if the group exists or not.

### Edit group

The owner of a group is given an option to edit the group page. This allows the owner to rename the group, rewrite the description or change the group to public/private.

### Invite users

If the group is private, members of the group have the option to invite new people into the group, using the ***Invite*** button. If a valid username is entered into the field, they get added to the group. If an invalid username is entered, the user is shown a *"user not found"* error.

### Subscribe / Unsubscribe

The user may choose to subscribe or unsubscribe to a group. If they are the owner of a group, they cannot unsubscribe. A message explaining this is shown instead of the unsubscribe button.

### Post new message

Users may post a message in a group they are in by pressing the ***New message*** button. An input field and preview field are shown to the user.
If the input field is empty, the user may not post the message. The preview box shows what the message will look like.

## Message feed

The user is shown all messages from groups they are subscribed to.


## Messages

Messages are formatted using GFM.

### Show / hide thread

Shows / hide all replies of a message. Reply messages are hidden by default.

### Reply

Users can reply to a message by pressing the ***Reply*** button. This will show the user an input field and a preview field.
If the input field is empty, the user may not post the reply. The message will be posted after the user clicks the ***Send*** button.
If the user does not want to post the reply, they may press the ***Cancel*** button to close the reply box.

### Edit

A user can edit their message by hovering over the hamburger menu of one of their messages and selecting ***Edit***. This shows the user an input field with their message in it and a preview field.
After the user is done editing their message, they can press the ***send*** button to save the changes made. The user can also discard all changes made by pressing the cancel button and closing the edit window.

### Delete

A user may delete their message by hovering over the hamburger menu of one of their messages and selecting ***Delete***.
This will open a confirmation window asking if the user is sure they want to delete their message.

___

# Backend

## Swagger

### https://alumni-network-fm.azurewebsites.net/swagger/index.html

## Group
### Post group
`POST /api/v1/group`

### Get all groups
`GET /api/v1/group`

### Get specific group
`GET /api/v1/group/{id}`

### Update group with specific Id
`PUT /api/v1/group/{id}`

### Add user to a group
`PUT /api/v1/group/{groupId}/add/{userId}`

### Remove a user from a group
`PUT /api/v1/group/{groupId}/remove/{userId}`

### Get all users in a group
`GET /api/v1/group/{groupId}/users`

### Get all messages from a group
`GET /api/v1/group/{groupId}/messages`

## Message
### Post message
`POST /api/v1/message`

### Get specific message
`GET /api/v1/message/{id}`

### Update a message
`PUT /api/v1/message/{id}`

### Delete a message
`DELETE /api/v1/message/{id}`

### Get all thread messages from a message
`GET /api/v1/message/{id}/threadmessage`

## User
### Post user
`POST /api/v1/user`

### Get all users
`GET /api/v1/user`

### Get specific user by Id
`GET /api/v1/user/{id}`

### Get specific user by username
`GET /api/v1/user/username/{username}`

### Update a user
`PUT /api/v1/user/{id}`

### Get all groups from a user
`GET /api/v1/user/{id}/groups`

### Get all messages from a user
`GET /api/v1/user/{id}/messages`

## ASP .NET Plugins
### AutoMapper
### AutoMapper.Extensions.Microsoft.DependencyInjection
### Microsoft.AspNetCore.Authentication.JwtBearer
### Microsoft.AspNetCore.Cors
### Microsoft.EntityFrameworkCore
### Microsoft.EntityFrameworkCore.SqlServer
### Newtonsoft.Json
### Swashbuckle.AspNetCore

