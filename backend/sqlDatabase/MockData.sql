USE [AlumniNetwork]
GO

INSERT INTO [dbo].[User]
           ([Username]
		   ,[ProfilePicture]
		   ,[Name]
           ,[LastName]
           ,[Status]
           ,[Bio]
           ,[Quote])
     VALUES
           ('MarkW',
		   NULL,
		   'Mark',
           'de Wit',
           'Working hard', 
		   'Hardcore fullstack developer',
		   'Yes'),

		   ('Ferryk',
		   NULL,
		   'Ferry',
           'Kruidenberg',
           'Performing some IT magic', 
		   'Hardcore fullstack developer',
		   'No'),

		   ('OG_Elmo',
		   NULL,
		   'Elmo',
           'Monster',
           '', 
		   'From the street. Sesame Street.',
		   ''),

		   ('ParkerP',
		   NULL,
		   'Peter',
           'Parker',
           '', 
		   'Not being Spiderman, thats for sure.',
		   ''),

		   ('MrBean',
		   NULL,
		   'Rowan',
           'Atkinson',
           'Being awesome', 
		   '',
		   'Teddy!'),

		   ('CamelotKing',
		   NULL,
		   'Monty',
           'Python',
           'Searching for the holy grail', 
		   '',
		   'A swallow could have carried them'),

		   ('Builderman42',
		   NULL,
		   'Bob',
           'the Builder',
           '', 
		   '',
		   'Can we fix it?'),

		   ('HeresJohny',
		   NULL,
		   'John',
           'Doe',
           'Foo', 
		   'Bar',
		   'Cant have test data without me.')
GO

INSERT INTO [dbo].[Group]
           ([IsPrivate]
           ,[CreatorId]
           ,[Name]
           ,[Description])
SELECT	0, [User].[Id], 'Public group', 'The first public group created'
FROM	[dbo].[User]
WHERE	[User].[Name] = 'Ferry';

INSERT INTO [dbo].[Group]
           ([IsPrivate]
           ,[CreatorId]
           ,[Name]
           ,[Description])
SELECT	1, [User].[Id], 'Sesame Street Gang', 'A private group'
FROM	[dbo].[User]
WHERE	[User].[Name] = 'Elmo';

INSERT INTO [dbo].[Group]
           ([IsPrivate]
           ,[CreatorId]
           ,[Name]
           ,[Description])
SELECT	0, [User].[Id], 'Another public group', 'Because we needed another public group.'
FROM	[dbo].[User]
WHERE	[User].[Name] = 'Mark';

GO


INSERT INTO [dbo].[Message]
           ([PostUserId]
           ,[MessageText]
           ,[TimePosted]
           ,[ReplyParentId]
           ,[TargetUserId]
           ,[TargetGroupId])
SELECT  [User].[Id], 'This is a test message', '2021-10-05 17:00:00.000', NULL, NULL, [Group].[Id]
FROM    [dbo].[User], [dbo].[Group]
WHERE   [User].[Name] = 'Mark' AND [Group].[Name] = 'Public group';

INSERT INTO [dbo].[Message]
           ([PostUserId]
           ,[MessageText]
           ,[TimePosted]
           ,[ReplyParentId]
           ,[TargetUserId]
           ,[TargetGroupId])
SELECT  [User].[Id], 'This is also test message', '2021-10-06 09:00:00.000', NULL, NULL, [Group].[Id]
FROM    [dbo].[User], [dbo].[Group]
WHERE   [User].[Name] = 'Ferry' AND [Group].[Name] = 'Another public group';

INSERT INTO [dbo].[Message]
           ([PostUserId]
           ,[MessageText]
           ,[TimePosted]
           ,[ReplyParentId]
           ,[TargetUserId]
           ,[TargetGroupId])
SELECT  [User].[Id], 'This test message is for the Sesame Street Gang', '2021-10-06 16:00:00.000', NULL, NULL, [Group].[Id]
FROM    [dbo].[User], [dbo].[Group]
WHERE   [User].[Name] = 'Elmo' AND [Group].[Name] = 'Sesame Street Gang';

INSERT INTO [dbo].[Message]
           ([PostUserId]
           ,[MessageText]
           ,[TimePosted]
           ,[ReplyParentId]
           ,[TargetUserId]
           ,[TargetGroupId])
SELECT  [User].[Id], 'This message is a reply on the first Sesame Street Message', '2021-10-07 16:00:00.000', [Message].[Id], NULL, [Group].[Id]
FROM    [dbo].[User], [dbo].[Message], [dbo].[Group]
WHERE   [User].[Name] = 'Elmo' AND [Message].[MessageText] = 'This test message is for the Sesame Street Gang' AND [Group].[Name] = 'Sesame Street Gang';

INSERT INTO [dbo].[Message]
           ([PostUserId]
           ,[MessageText]
           ,[TimePosted]
           ,[ReplyParentId]
           ,[TargetUserId]
           ,[TargetGroupId])
SELECT  [User].[Id], 'A reply to a test message', '2021-10-10 16:00:00.000', [Message].[Id], NULL, [Group].[Id]
FROM    [dbo].[User], [dbo].[Message], [dbo].[Group]
WHERE   [User].[Name] = 'Mark' AND [Message].[MessageText] = 'This is also test message' AND [Group].[Name] = 'Another public group';


INSERT INTO [dbo].[GroupUser]
           ([UsersId]
           ,[GroupsId])
SELECT	[User].[Id], [Group].[Id]
FROM	[dbo].[User], [dbo].[Group]
WHERE	[User].[Name] = 'Ferry' AND [Group].[Name] = 'Public group';

INSERT INTO [dbo].[GroupUser]
           ([UsersId]
           ,[GroupsId])
SELECT	[User].[Id], [Group].[Id]
FROM	[dbo].[User], [dbo].[Group]
WHERE	[User].[Name] = 'Ferry' AND [Group].[Name] = 'Another public group';

INSERT INTO [dbo].[GroupUser]
           ([UsersId]
           ,[GroupsId])
SELECT	[User].[Id], [Group].[Id]
FROM	[dbo].[User], [dbo].[Group]
WHERE	[User].[Name] = 'Mark' AND [Group].[Name] = 'Public group';

INSERT INTO [dbo].[GroupUser]
           ([UsersId]
           ,[GroupsId])
SELECT	[User].[Id], [Group].[Id]
FROM	[dbo].[User], [dbo].[Group]
WHERE	[User].[Name] = 'Mark' AND [Group].[Name] = 'Another public group';

INSERT INTO [dbo].[GroupUser]
           ([UsersId]
           ,[GroupsId])
SELECT	[User].[Id], [Group].[Id]
FROM	[dbo].[User], [dbo].[Group]
WHERE	[User].[Name] = 'Elmo' AND [Group].[Name] = 'Sesame Street Gang';


INSERT INTO [dbo].[GroupUser]
           ([UsersId]
           ,[GroupsId])
SELECT	[User].[Id], [Group].[Id]
FROM	[dbo].[User], [dbo].[Group]
WHERE	[User].[Name] = 'Rowan' AND [Group].[Name] = 'Public group';


INSERT INTO [dbo].[GroupUser]
           ([UsersId]
           ,[GroupsId])
SELECT	[User].[Id], [Group].[Id]
FROM	[dbo].[User], [dbo].[Group]
WHERE	[User].[Name] = 'Peter' AND [Group].[Name] = 'Public group';


INSERT INTO [dbo].[GroupUser]
           ([UsersId]
           ,[GroupsId])
SELECT	[User].[Id], [Group].[Id]
FROM	[dbo].[User], [dbo].[Group]
WHERE	[User].[Name] = 'Monty' AND [Group].[Name] = 'Public group';

GO

