﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlumniNetworkBackend
{
    public class GroupReadDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsPrivate { get; set; }
		public string Description { get; set; }
        public int CreatorId { get; set; }

        public List<int> Users { get; set; }
        public List<int> Messages { get; set; }
    }
}
