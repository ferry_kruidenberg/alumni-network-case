﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlumniNetworkBackend
{
    public class GroupEditDTO
    {
        public int Id { get; set; }
		public bool IsPrivate { get; set; }
        public string Name { get; set; }
		public string Description { get; set; }
    }
}
