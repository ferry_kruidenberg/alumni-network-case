﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlumniNetworkBackend
{
    public class MessageCreateDTO
    {
		public int PostUserId { get; set; }
		public string MessageText { get; set; }

		public int? ReplyParentId { get; set; }
		
		public int? TargetUserId { get; set; }
		
		public int? TargetGroupId { get; set; }
	}
}
