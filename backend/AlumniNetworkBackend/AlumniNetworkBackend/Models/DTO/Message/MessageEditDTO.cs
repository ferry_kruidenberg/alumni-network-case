﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlumniNetworkBackend
{
    public class MessageEditDTO
    {
        public int Id { get; set; }
		public string MessageText { get; set; }
	}
}
