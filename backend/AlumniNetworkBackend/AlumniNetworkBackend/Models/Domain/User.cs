﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AlumniNetworkBackend
{
	public class User
	{
		public int Id { get; set; }
		[MaxLength(20)]
		public string UserName { get; set; }
		[MaxLength(50)]
		public string Name { get; set; }
		[MaxLength(50)]
		public string LastName { get; set; }
		[MaxLength(100)]
		public string Status { get; set; }
		[MaxLength(500)]
		public string Bio { get; set; }
		[MaxLength(100)]
		public string Quote { get; set; }

		public byte[] ProfilePicture { get; set; }

		public ICollection<Group> Groups { get; set; }

	}
}