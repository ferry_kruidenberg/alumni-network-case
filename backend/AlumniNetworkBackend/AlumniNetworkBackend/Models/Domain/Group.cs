﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AlumniNetworkBackend
{
	public class Group
	{
		public int Id { get; set; }
		public bool IsPrivate { get; set; }
		public int CreatorId { get; set; }
		[MaxLength(50)]
		public string Name { get; set; }
		[MaxLength(500)]
		public string Description { get; set; }

		public ICollection<User> Users { get; set; }

		public ICollection<Message> Messages { get; set; }
	}
}