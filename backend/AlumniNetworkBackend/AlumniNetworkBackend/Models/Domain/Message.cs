﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AlumniNetworkBackend
{
	public class Message
	{
		public int Id { get; set; }
		public int PostUserId { get; set; }

		[MaxLength(500)]
		public string MessageText { get; set; }
		public DateTime TimePosted { get; set; }

		public int? ReplyParentId { get; set; }
		public Message ReplyParent { get; set; }

		public ICollection<Message> ThreadMessages { get; set; }


		public int? TargetUserId { get; set; }
		public User TargetUser { get; set; }

		public int? TargetGroupId { get; set; }
		public Group TargetGroup { get; set; }

		
	}
}