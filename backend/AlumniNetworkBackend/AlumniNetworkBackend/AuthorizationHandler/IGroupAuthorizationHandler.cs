﻿using System;
using System.Threading.Tasks;

namespace AlumniNetworkBackend
{
    public interface IGroupAuthorizationHandler
    {
        public Task<bool> AuthorizeGroupMember(Group group, string accessToken);
        public Task<bool> AuthorizeGroupDeleteUser(Group group, int userId, string accessToken);
        public Task<bool> AuthorizeGroupRead(Group group, string accessToken);
        public Task<bool> AuthorizeGroupCreator(Group group, string accessToken);
    }
}
