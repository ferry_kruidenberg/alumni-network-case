﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mime;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication;
using System.IdentityModel.Tokens.Jwt;

namespace AlumniNetworkBackend
{
    public class GroupAuthorizationHandler : IGroupAuthorizationHandler
    {
        private readonly IUserService _userService;

        public GroupAuthorizationHandler(IUserService userService)
        {
            _userService = userService;
        }

        /// <summary>
        /// Checks if the user is a member of the group.
        /// </summary>
        /// <param name="group"></param>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        public async Task<bool> AuthorizeGroupMember(Group group, string accessToken)
        {
            
            var handler = new JwtSecurityTokenHandler();
            var jwtSecurityToken = handler.ReadJwtToken(accessToken);

            object username;
            jwtSecurityToken.Payload.TryGetValue("preferred_username", out username);

            
            User user = await _userService.UserFromUsername(username.ToString());


            if (group.Users.Contains(user))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Delete a specific user from a group. Users can only delete themselves. Creator can only delete other users.
        /// </summary>
        /// <param name="group"></param>
        /// <param name="userId"></param>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        public async Task<bool> AuthorizeGroupDeleteUser(Group group, int userId, string accessToken)
        {
            //Creator can't delete themselves from a group.
            if(group.CreatorId == userId)
            {
                return false;
            }
            var handler = new JwtSecurityTokenHandler();
            var jwtSecurityToken = handler.ReadJwtToken(accessToken);

            object username;
            jwtSecurityToken.Payload.TryGetValue("preferred_username", out username);


            User user = await _userService.UserFromUsername(username.ToString());

            if(group.CreatorId == user.Id || user.Id == userId)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Checks if the group is public. If it's private only allow users in the group.
        /// </summary>
        /// <param name="group"></param>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        public async Task<bool> AuthorizeGroupRead(Group group, string accessToken)
        {
            if (!group.IsPrivate)
            {
                return true;
            }
            else
            {
                return await AuthorizeGroupMember(group, accessToken);
            }
        }

        /// <summary>
        /// Checks if the current user is the group's creator
        /// </summary>
        /// <param name="group"></param>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        public async Task<bool> AuthorizeGroupCreator(Group group, string accessToken)
        {
            var handler = new JwtSecurityTokenHandler();
            var jwtSecurityToken = handler.ReadJwtToken(accessToken);

            object username;
            jwtSecurityToken.Payload.TryGetValue("preferred_username", out username);


            User user = await _userService.UserFromUsername(username.ToString());

            if (group.CreatorId == user.Id)
            {
                return true;
            }

            return false;
        }
    }
}


public class SameAuthorRequirement : IAuthorizationRequirement { }