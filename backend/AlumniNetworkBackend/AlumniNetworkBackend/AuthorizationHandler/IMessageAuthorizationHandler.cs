﻿using System;
using System.Threading.Tasks;

namespace AlumniNetworkBackend
{
    public interface IMessageAuthorizationHandler
    {
        public Task<bool> AuthorizeMessage(Message message, string accessToken);
        public Task<bool> AuthorizeMessageCreator(Message message, string accessToken);
    }
}
