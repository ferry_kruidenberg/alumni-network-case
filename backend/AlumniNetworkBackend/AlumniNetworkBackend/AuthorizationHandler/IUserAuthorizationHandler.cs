﻿using System;
using System.Threading.Tasks;

namespace AlumniNetworkBackend
{
    public interface IUserAuthorizationHandler
    {
        public Task<bool> AuthorizeUser(int id, string accessToken);
    }
}
