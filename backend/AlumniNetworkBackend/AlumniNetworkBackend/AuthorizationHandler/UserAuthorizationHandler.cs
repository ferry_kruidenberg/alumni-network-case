﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Threading.Tasks;

namespace AlumniNetworkBackend
{
    public class UserAuthorizationHandler : IUserAuthorizationHandler
    {
        private readonly IUserService _userService;

        public UserAuthorizationHandler(IUserService userService)
        {
            _userService = userService;
        }

        /// <summary>
        /// Check if the user is who they claim to be
        /// </summary>
        /// <param name="id"></param>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        public async Task<bool> AuthorizeUser(int id, string accessToken)
        {
            var handler = new JwtSecurityTokenHandler();
            var jwtSecurityToken = handler.ReadJwtToken(accessToken);

            object username;
            jwtSecurityToken.Payload.TryGetValue("preferred_username", out username);

            User user = await _userService.UserFromUsername(username.ToString());


            if (user.Id == id)
            {
                return true;
            }

            return false;
        }
    }
}
