﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Threading.Tasks;

namespace AlumniNetworkBackend
{
    public class MessageAuthorisationHandler : IMessageAuthorizationHandler
    {
        private readonly IUserService _userService;
        private readonly IGroupService _groupService;

        public MessageAuthorisationHandler(IUserService userService, IGroupService groupService)
        {
            _userService = userService;
            _groupService = groupService;
        }

        /// <summary>
        /// Checks if the user is posting as themself in a group they are part of
        /// </summary>
        /// <param name="message"></param>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        public async Task<bool> AuthorizeMessage(Message message, string accessToken)
        {
            var handler = new JwtSecurityTokenHandler();
            var jwtSecurityToken = handler.ReadJwtToken(accessToken);

            object username;
            jwtSecurityToken.Payload.TryGetValue("preferred_username", out username);


            User user = await _userService.UserFromUsername(username.ToString());
            Group group = await _groupService.GetGroupAsync(message.TargetGroup.Id);

            if (message.TargetUser == user || group.Users.Contains(user))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Checks if the user is the creator of the message to edit/delete
        /// </summary>
        /// <param name="message"></param>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        public async Task<bool> AuthorizeMessageCreator(Message message, string accessToken)
        {
            var handler = new JwtSecurityTokenHandler();
            var jwtSecurityToken = handler.ReadJwtToken(accessToken);

            object username;
            jwtSecurityToken.Payload.TryGetValue("preferred_username", out username);


            User user = await _userService.UserFromUsername(username.ToString());

            if (message.PostUserId == user.Id)
            {
                return true;
            }

            return false;
        }
    }
}
