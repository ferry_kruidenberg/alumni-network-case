﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mime;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;

namespace AlumniNetworkBackend
{
    [Authorize]
    [Route("api/v1/user")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class UserController : ControllerBase
    {
        private readonly IMapper _mapper;

        private readonly IUserService _userService;

        private readonly IUserAuthorizationHandler _userAuthorizationHandler;

        public UserController(IMapper mapper, IUserService userService, IUserAuthorizationHandler userAuthorizationHandler)
        {
            _mapper = mapper;
            _userService = userService;
            _userAuthorizationHandler = userAuthorizationHandler;
        }

        /// <summary>
        /// Add a new user to the database
        /// </summary>
        /// <param name="userDTO"></param>
        /// <returns></returns>
        /// <response code="201">User has been created</response>
        /// <response code="400">Invalid payload</response>
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpPost]
        public async Task<ActionResult<UserReadDTO>> PostUser(UserCreateDTO userDTO)
        {
            string accessToken = await GetAccessToken();
            var handler = new JwtSecurityTokenHandler();
            var jwtSecurityToken = handler.ReadJwtToken(accessToken);

            object username;
            jwtSecurityToken.Payload.TryGetValue("preferred_username", out username);

            User domainUser = _mapper.Map<User>(userDTO);
            domainUser.UserName = username.ToString();
            await _userService.CreateUserAsync(domainUser);

            return CreatedAtAction("GetUser", new { id = domainUser.Id },
                _mapper.Map<UserReadDTO>(domainUser));
        }

        /// <summary>
        /// Get all users from the database
        /// </summary>
        /// <returns></returns>
        /// <response code="200">Returns all users</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<UserReadDTO>>> GetUsers()
        {
            return _mapper.Map<List<UserReadDTO>>(await _userService.GetAllUsersAsync());
        }

        /// <summary>
        /// Get a user from the database given an id
        /// </summary>
        /// <returns></returns>
        /// <response code="200">Returns specific user</response>
        /// <response code="404">User does not exist</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpGet("{id}")]
        public async Task<ActionResult<UserReadDTO>> GetUser(int id)
        {
            if (!_userService.UserExists(id))
            {
                return NotFound();
            }
            User user = await _userService.GetSpecificUserAsync(id);
            return _mapper.Map<UserReadDTO>(user);
        }

        /// <summary>
        /// Get the user from given username
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        /// <response code="200">Returns specific user</response>
        /// <response code="404">User does not exist</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpGet("username/{username}")]
        public async Task<ActionResult<UserReadDTO>> GetUserByUsername(string username)
        {
            User user = await _userService.UserFromUsername(username);

            if (user == null)
            {
                return NotFound();
            }

            return _mapper.Map<UserReadDTO>(user);
        }


        /// <summary>
        /// Get currently logged-in user from the database
        /// </summary>
        /// <returns></returns>
        /// <response code="200">Returns the current user</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpGet("self")]
        public async Task<ActionResult<UserReadDTO>> GetUserFromJwt()
        {
            var accessToken = await GetAccessToken();
            var handler = new JwtSecurityTokenHandler();
            var jwtSecurityToken = handler.ReadJwtToken(accessToken);

            object username;
            jwtSecurityToken.Payload.TryGetValue("preferred_username", out username);

            User user = await _userService.UserFromUsername(username.ToString());

            if (user == null)
            {
                return NotFound();
            }
            return _mapper.Map<UserReadDTO>(user);
        }



        /// <summary>
        /// Input a user object to update user with specified id in the database
        /// </summary>
        /// <param name="id"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        /// <response code="204">User has successfully been updated</response>
        /// <response code="400">Invalid payload or id does not match</response>
        /// <response code="404">User does not exist</response>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpPut("{id}")]
        public async Task<ActionResult> PutUser(int id, UserEditDTO user)
        {
            if(id != user.Id)
            {
                return BadRequest();
            }

            if (!_userService.UserExists(id))
            {
                return NotFound();
            }

            var accessToken = await GetAccessToken();

            if (await _userAuthorizationHandler.AuthorizeUser(id, accessToken))
            {
                await _userService.UpdateUserAsync(user);

                return NoContent();
            } else
            {
                return new ForbidResult();
            }
        }

        /// <summary>
        /// Get all the groups from the user by its id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <response code="200">Returns all groups the user is in</response>
        /// <response code="404">User does not exist</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpGet("{id}/groups")]
        public async Task<ActionResult<IEnumerable<GroupReadDTO>>> GetGroups(int id)
        {
            if (!_userService.UserExists(id))
            {
                return NotFound();
            }

            var accessToken = await GetAccessToken();

            if (await _userAuthorizationHandler.AuthorizeUser(id, accessToken))
            {
                return _mapper.Map<List<GroupReadDTO>>(await _userService.GetAllGroupsFromUserAsync(id));
            }
            else
            {
                return new ForbidResult();
            }
        }

        /// <summary>
        /// Get all the messages from the user by its id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <response code="200">Returns all messages from the user</response>
        /// <response code="404">User does not exist</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpGet("{id}/messages")]
        public async Task<ActionResult<IEnumerable<MessageReadDTO>>> GetMessages(int id)
        {
            if (!_userService.UserExists(id))
            {
                return NotFound();
            }

            var accessToken = await GetAccessToken();

            if (await _userAuthorizationHandler.AuthorizeUser(id, accessToken))
            {
                return _mapper.Map<List<MessageReadDTO>>(await _userService.GetAllMessagesFromUserAsync(id));
            }
            else
            {
                return new ForbidResult();
            }
        }


        public async Task<string> GetAccessToken()
        {
            return await HttpContext.GetTokenAsync("access_token");
        }
    }

}
