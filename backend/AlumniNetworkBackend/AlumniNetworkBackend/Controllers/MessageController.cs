﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mime;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;

namespace AlumniNetworkBackend
{
    [Authorize]
    [Route("api/v1/message")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class MessageController : ControllerBase
    {

        private readonly IMapper _mapper;

        private readonly IMessageService _messageService;

        private readonly IMessageAuthorizationHandler _messageAuthorizationHandler;

        public MessageController(IMapper mapper, IMessageService messageService, IMessageAuthorizationHandler messageAuthorizationHandler)
        {
            _mapper = mapper;
            _messageService = messageService;
            _messageAuthorizationHandler = messageAuthorizationHandler;
        }

        /// <summary>
        /// Add a message to the database
        /// </summary>
        /// <param name="messageDTO"></param>
        /// <returns></returns>
        /// <response code="201">Message has been created</response>
        /// <response code="400">Invalid payload</response>
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpPost]
        public async Task<ActionResult<MessageReadDTO>> Postmessage(MessageCreateDTO messageDTO)
        {
            Message domainMessage = _mapper.Map<Message>(messageDTO);

            var accessToken = await GetAccessToken();

            if (await _messageAuthorizationHandler.AuthorizeMessageCreator(domainMessage, accessToken))
            {
                domainMessage.TimePosted = DateTime.Now;
                await _messageService.CreateMessageAsync(domainMessage);

                return CreatedAtAction("GetMessage", new { id = domainMessage.Id },
                    _mapper.Map<MessageReadDTO>(domainMessage));
            }
            else
            {
                return new ForbidResult();
            }
        }

        /// <summary>
        /// Get a message from the database given an id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <response code="200">Message has been created</response>
        /// <response code="404">Message does not exist</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpGet("{id}")]
        public async Task<ActionResult<MessageReadDTO>> GetMessage(int id)
        {
            if (!_messageService.MessageExists(id))
            {
                return NotFound();
            }

            Message message = await _messageService.GetSpecificMessageAsync(id);

            var accessToken = await GetAccessToken();

            if (await _messageAuthorizationHandler.AuthorizeMessage(message, accessToken))
            {
                return _mapper.Map<MessageReadDTO>(message);
            }
            else
            {
                return new ForbidResult();
            }
        }

        /// <summary>
        /// Get all messages from a thread from the database given an id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <response code="200">Return all thread messages from message</response>=
        /// <response code="404">Message does not exist</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpGet("{id}/threadmessage")]
        public async Task<ActionResult<List<MessageReadDTO>>> GetAllThreadMessage(int id)
        {
            if (!_messageService.MessageExists(id))
            {
                return NotFound();
            }

            Message message = await _messageService.GetSpecificMessageAsync(id);

            var accessToken = await GetAccessToken();

            if (await _messageAuthorizationHandler.AuthorizeMessage(message, accessToken))
            {
                return _mapper.Map<List<MessageReadDTO>>(await _messageService.GetAllTreadMessages(id));
            }
            else
            {
                return new ForbidResult();
            }
        }

        /// <summary>
        /// Input a message object to update message with specified id in the database
        /// </summary>
        /// <param name="id"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        /// <response code="204">Message has been updated</response>
        /// <response code="400">Invalid payload</response>
        /// <response code="404">Message does not exist</response>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpPut("{id}")]
        public async Task<ActionResult> PutMessage(int id, MessageEditDTO message)
        {
            if (id != message.Id)
            {
                return BadRequest();
            }

            if (!_messageService.MessageExists(id))
            {
                return NotFound();
            }

            Message domainMessage = await _messageService.GetSpecificMessageAsync(id);

            var accessToken = await GetAccessToken();

            if (await _messageAuthorizationHandler.AuthorizeMessageCreator(domainMessage, accessToken))
            {
                await _messageService.UpdateMessageAsync(message);

                return NoContent();
            }
            else
            {
                return new ForbidResult();
            }
        }

        /// <summary>
        /// Delete a message from the database
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <response code="200">Message has been deleted</response>
        /// <response code="404">Message does not exist</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMessage(int id)
        {
            if (!_messageService.MessageExists(id))
            {
                return NotFound();
            }

            Message message = await _messageService.GetSpecificMessageAsync(id);

            var accessToken = await GetAccessToken();

            if (await _messageAuthorizationHandler.AuthorizeMessageCreator(message, accessToken))
            {
                await _messageService.DeleteMessageAsync(id);

                return NoContent();
            }
            else
            {
                return new ForbidResult();
            }
        }

        public async Task<string> GetAccessToken()
        {
            return await HttpContext.GetTokenAsync("access_token");
        }
    }
}
