﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mime;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.AspNetCore.Http;

namespace AlumniNetworkBackend
{
    [Authorize]
    [Route("api/v1/group")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class GroupController : ControllerBase
    {
        private readonly IMapper _mapper;

        private readonly IGroupService _groupService;

        private readonly IUserService _userService;

        private readonly IGroupAuthorizationHandler _groupAuthorizationHandler;

        public GroupController(IMapper mapper, IGroupService groupService, IUserService userService, IGroupAuthorizationHandler groupAuthorizationHandler)
        {
            _mapper = mapper;
            _groupService = groupService;
            _userService = userService;
            _groupAuthorizationHandler = groupAuthorizationHandler;
        }

        /// <summary>
        /// Add a new group to the database
        /// </summary>
        /// <param name="groupDTO"></param>
        /// <returns></returns>
        /// <response code="201">Item has been created</response>
        /// <response code="400">Invalid payload</response>
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpPost]
        public async Task<ActionResult<GroupReadDTO>> PostGroup(GroupCreateDTO groupDTO)
        {
            Group domainGroup = _mapper.Map<Group>(groupDTO);

            string accessToken = await GetAccessToken();
            var handler = new JwtSecurityTokenHandler();
            var jwtSecurityToken = handler.ReadJwtToken(accessToken);

            object username;
            jwtSecurityToken.Payload.TryGetValue("preferred_username", out username);

            User user = await _userService.UserFromUsername(username.ToString());

            await _groupService.CreateGroupAsync(domainGroup, user.Id);

            return CreatedAtAction("GetGroup", new { id = domainGroup.Id },
                _mapper.Map<GroupReadDTO>(domainGroup));
        }

        /// <summary>
        /// Returns a list of all public groups
        /// </summary>
        /// <returns></returns>
        /// <response code="200">Returns all public groups</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<GroupReadDTO>>> GetPublicGroups()
        {
            return _mapper.Map<List<GroupReadDTO>>(await _groupService.GetAllPublicGroupsAsync());
        }

        /// <summary>
        /// Returns a specific group
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <response code="200">Returns specific group</response>
        /// <response code="404">Group with id does not exist</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpGet("{id}")]
        public async Task<ActionResult<GroupReadDTO>> GetGroup(int id)
        {
            if (!_groupService.GroupExists(id))
            {
                return NotFound();
            }

            var accessToken = await GetAccessToken();
            Group group = await _groupService.GetGroupAsync(id);


            if (await _groupAuthorizationHandler.AuthorizeGroupRead(group, accessToken))
            {

                return _mapper.Map<GroupReadDTO>(group);

            }
            else
            {
                return new ForbidResult();
            }
        }

        /// <summary>
        /// Update a group with a specific Id.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="group"></param>
        /// <returns></returns>
        /// <response code="204">Group has been updated</response>
        /// <response code="400">Invalid payload</response>
        /// <response code="404">Group not found</response>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutGroup(int id, GroupEditDTO group)
        {
            var accessToken = await GetAccessToken();

            if (id != group.Id)
            {
                return BadRequest();
            }

            if (!_groupService.GroupExists(id))
            {
                return NotFound();
            }

            Group domainGroup = await _groupService.GetGroupAsync(id);

            if (await _groupAuthorizationHandler.AuthorizeGroupCreator(domainGroup, accessToken))
            {

                await _groupService.UpdateGroupAsync(group);

                return NoContent();

            } else
            {
                return new ForbidResult();
            }
        }

        /// <summary>
        /// Add a user to a group
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="groupId"></param>
        /// <returns></returns>
        /// <response code="204">User has been added</response>
        /// <response code="404">Group or user does not exist</response>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpPut("{groupId}/add/{userId}")]
        public async Task<IActionResult> AddUserToGroup(int userId, int groupId)
        {
            if (!_groupService.GroupExists(groupId))
            {
                return NotFound();
            }

            if (!_userService.UserExists(userId))
            {
                return NotFound();
            }

            Group group = await _groupService.GetGroupAsync(groupId);
            var accessToken = await GetAccessToken();

            if (await _groupAuthorizationHandler.AuthorizeGroupRead(group, accessToken))
            {
                await _groupService.AddUserToGroupAsync(userId, groupId);

                return NoContent();

            }
            else
            {
                return new ForbidResult();
            }
        }

        /// <summary>
        /// Removes a specific user from a group
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="groupId"></param>
        /// <returns></returns>
        /// <response code="204">User has been removed</response>
        /// <response code="404">Group or user does not exist</response>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpPut("{groupId}/remove/{userId}")]
        public async Task<IActionResult> RemoveUserFromGroup(int userId, int groupId)
        {
            if (!_groupService.GroupExists(groupId))
            {
                return NotFound();
            }

            if (!_userService.UserExists(userId))
            {
                return NotFound();
            }

            var accessToken = await GetAccessToken();
            Group group = await _groupService.GetGroupAsync(groupId);

            if (await _groupAuthorizationHandler.AuthorizeGroupDeleteUser(group, userId, accessToken))
            {
                await _groupService.RemoveUserFromGroupAsync(userId, groupId);

                return NoContent();
            }
            else
            {
                return new ForbidResult();
            }
        }

        /// <summary>
        /// Returns a list of all users in a group
        /// </summary>
        /// <param name="groupId"></param>
        /// <returns></returns>
        /// <response code="200">Returns a list of all users of the group</response>
        /// <response code="404">Group does not exist</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpGet("{groupId}/users")]
        public async Task<ActionResult<IEnumerable<UserReadDTO>>> GetUsersFromGroup(int groupId)
        {
            if (!_groupService.GroupExists(groupId))
            {
                return NotFound();
            }

            var accessToken = await GetAccessToken();
            Group group = await _groupService.GetGroupAsync(groupId);

            if (await _groupAuthorizationHandler.AuthorizeGroupRead(group, accessToken))
            {
                return _mapper.Map<List<UserReadDTO>>(await _groupService.GetAllUsersInGroupAsync(groupId));
            }
            else
            {
                return new ForbidResult();
            }
        }

        /// <summary>
        /// Returns all the messages from a group
        /// </summary>
        /// <param name="groupId"></param>
        /// <returns></returns>
        /// <response code="200">Returns a list of all messages from the group</response>
        /// <response code="404">Group does not exist</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpGet("{groupId}/messages")]
        public async Task<ActionResult<IEnumerable<MessageReadDTO>>> GetMessagesFromGroup(int groupId)
        {
            if (!_groupService.GroupExists(groupId))
            {
                return NotFound();
            }

            var accessToken = await GetAccessToken();
            Group group = await _groupService.GetGroupAsync(groupId);


            if (await _groupAuthorizationHandler.AuthorizeGroupRead(group, accessToken))
            {
                return _mapper.Map<List<MessageReadDTO>>(await _groupService.GetAllMessagesFromGroupAsync(groupId));
            }
            else
            {
                return new ForbidResult();
            }
        }

        public async Task<string> GetAccessToken()
        {
            return await HttpContext.GetTokenAsync("access_token");
        }
    }
}
