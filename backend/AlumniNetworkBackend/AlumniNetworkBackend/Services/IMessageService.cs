﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AlumniNetworkBackend
{
    public interface IMessageService
    {
        public Task<Message> GetSpecificMessageAsync(int id);
        public Task<IEnumerable<Message>> GetAllTreadMessages(int id);
        public Task<Message> CreateMessageAsync(Message message);
        public Task UpdateMessageAsync(MessageEditDTO message);
        public Task DeleteMessageAsync(int id);
        public bool MessageExists(int id);
    }
}
