﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AlumniNetworkBackend
{
    public interface IGroupService
    {
        public Task<IEnumerable<Group>> GetAllPublicGroupsAsync();
        public Task<Group> GetGroupAsync(int id);
        public Task<Group> CreateGroupAsync(Group group, int userId);
        public Task UpdateGroupAsync(GroupEditDTO group);
        public Task<Group> AddUserToGroupAsync(int userId, int groupId);
        public Task<Group> RemoveUserFromGroupAsync(int userId, int groupId);
        public Task<IEnumerable<User>> GetAllUsersInGroupAsync(int id);
        public Task<IEnumerable<Message>> GetAllMessagesFromGroupAsync(int id);
        public bool GroupExists(int id);
    }
}
