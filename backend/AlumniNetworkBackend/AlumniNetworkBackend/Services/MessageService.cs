﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlumniNetworkBackend
{
    public class MessageService : IMessageService
    {
        private readonly AlumniNetworkDbContext _context;

        public MessageService(AlumniNetworkDbContext context)
        {
            _context = context;
        }

        public async Task<Message> CreateMessageAsync(Message message)
        {
            _context.Message.Add(message);
            await _context.SaveChangesAsync();

            return message;
        }

        public async Task<Message> GetSpecificMessageAsync(int id)
        {
            return await _context.Message
            .Include(m => m.TargetGroup)
            .Include(m => m.TargetUser)
            .Include(m => m.ThreadMessages)
            .FirstAsync(m => m.Id == id);

        }


        public async Task<IEnumerable<Message>> GetAllTreadMessages(int id)
        {
            var message = await _context.Message
                .Include(m => m.TargetGroup)
                .Include(m => m.TargetUser)
                .Include(m => m.ThreadMessages).
                FirstAsync(m => m.Id == id);
            return message.ThreadMessages.ToList();
        }

        public async Task UpdateMessageAsync(MessageEditDTO messageEdit)
        {
            Message message = await GetSpecificMessageAsync(messageEdit.Id);
            message.MessageText = messageEdit.MessageText;
            _context.Entry(message).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }


        public async Task DeleteMessageAsync(int id)
        {
            var message = await _context.Message.FindAsync(id);
            _context.Message.Remove(message);
            await _context.SaveChangesAsync();
        }

        public bool MessageExists(int id)
        {
            return _context.Message.Any(m => m.Id == id);
        }
    }
}
