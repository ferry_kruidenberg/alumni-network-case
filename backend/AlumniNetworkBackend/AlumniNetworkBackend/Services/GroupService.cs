﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mime;
using System.Collections.ObjectModel;

namespace AlumniNetworkBackend
{
    class GroupService : IGroupService
    {
        private readonly AlumniNetworkDbContext _context;

        public GroupService(AlumniNetworkDbContext context)
        {
            _context = context;
        }

        public async Task<Group> CreateGroupAsync(Group group, int userId)
        {
            User user = await _context.User.FindAsync(userId);
            group.Users = new Collection<User>();
            group.Users.Add(user);
            group.CreatorId = userId;
            _context.Group.Add(group);

            await _context.SaveChangesAsync();

            return group;
        }

        public async Task<IEnumerable<Group>> GetAllPublicGroupsAsync()
        {
            return await _context.Group.Include(g => g.Users).Include(g => g.Messages).Where(g => g.IsPrivate == false).ToListAsync();
        }

        public async Task<Group> GetGroupAsync(int id)
        {
            return await _context.Group
                .Include(g => g.Users)
                .Include(g => g.Messages)
                .FirstAsync(g => g.Id == id);
        }

        public async Task UpdateGroupAsync(GroupEditDTO groupEdit)
        {
            Group group = await GetGroupAsync(groupEdit.Id);
            group.IsPrivate = groupEdit.IsPrivate;
            group.Description = groupEdit.Description;
            group.Name = groupEdit.Name;

            _context.Entry(group).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task<Group> AddUserToGroupAsync(int userId, int groupId)
        {
            Group group = await GetGroupAsync(groupId);
            User user = await _context.User.FindAsync(userId);
            group.Users.Add(user);

            await _context.SaveChangesAsync();
            return group;
        }

        public async Task<Group> RemoveUserFromGroupAsync(int userId, int groupId)
        {
            Group group = await GetGroupAsync(groupId);
            User user = await _context.User.FindAsync(userId);

            group.Users.Remove(user);
            await _context.SaveChangesAsync();
            return group;
        }

        public async Task<IEnumerable<User>> GetAllUsersInGroupAsync(int id)
        {
            Group group = await _context.Group.FindAsync(id);
            return await _context.User.Include(u => u.Groups).Where(u => u.Groups.Contains(group)).ToListAsync();

        }

        public async Task<IEnumerable<Message>> GetAllMessagesFromGroupAsync(int id)
        {
            Group group = await _context.Group.FindAsync(id);
            return await _context.Message.Include(m => m.TargetGroup).Where(m => m.TargetGroup == group).ToListAsync();
        }

        public bool GroupExists(int id)
        {
            return _context.Group.Any(g => g.Id == id);
        }
    }
}
