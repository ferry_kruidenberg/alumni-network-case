﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AlumniNetworkBackend
{
    public interface IUserService
    {
        public Task<IEnumerable<User>> GetAllUsersAsync();
        public Task<User> GetSpecificUserAsync(int id);
        public Task<User> CreateUserAsync(User user);
        public Task<IEnumerable<Group>> GetAllGroupsFromUserAsync(int id);
        public Task<IEnumerable<Message>> GetAllMessagesFromUserAsync(int id);
        public Task UpdateUserAsync(UserEditDTO user);
        public Task<User> UserFromUsername(string username);
        public bool UserExists(int id);
    }
}
