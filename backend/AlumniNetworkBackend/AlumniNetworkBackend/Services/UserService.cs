﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlumniNetworkBackend
{
    public class UserService : IUserService
    {
        private readonly AlumniNetworkDbContext _context;

        public UserService(AlumniNetworkDbContext context)
        {
            _context = context;
        }

        public async Task<User> CreateUserAsync(User user)
        {
            _context.User.Add(user);
            await _context.SaveChangesAsync();

            return user;
        }

        public async Task<User> GetSpecificUserAsync(int id)
        {
            return await _context.User.Include(u => u.Groups)
                .FirstAsync(u => u.Id == id);
        }

        public async Task<IEnumerable<User>> GetAllUsersAsync()
        {
            return await _context.User.Include(u => u.Groups).ToListAsync();
        }

        public async Task<IEnumerable<Group>> GetAllGroupsFromUserAsync(int id)
        {
            var user = await _context.User.FindAsync(id);
            return await _context.Group
                .Include(g => g.Users)
                .Include(g => g.Messages)
                .Where(g => g.Users.Contains(user))
                .ToListAsync();
        }

        public async Task<IEnumerable<Message>> GetAllMessagesFromUserAsync(int id)
        {
            var user = await _context.User.FindAsync(id);
            var groupsFromUser = await this.GetAllGroupsFromUserAsync(id);
            return await _context.Message.Where(m => m.TargetUser == user || groupsFromUser.Contains(m.TargetGroup)).ToListAsync();
        }

        public async Task UpdateUserAsync(UserEditDTO userEdit)
        {
            User user = await GetSpecificUserAsync(userEdit.Id);
            user.Name = userEdit.Name;
            user.LastName = userEdit.LastName;
            user.Bio = userEdit.Bio;
            user.Status = userEdit.Status;
            user.Quote = userEdit.Quote;

            //if the user sends an invalid profile picture, the picture is set to null
            try
            {
                user.ProfilePicture = Convert.FromBase64String(userEdit.ProfilePicture.Split(',')[1]);
            } catch
            {
                user.ProfilePicture = null;
            }
            _context.Entry(user).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task<User> UserFromUsername(string username)
        {
            return await _context.User.Include(u => u.Groups).FirstOrDefaultAsync(u => u.UserName == username);
            
        }

        public bool UserExists(int id)
        {
            return _context.User.Any(u => u.Id == id);
        }
    }
}
