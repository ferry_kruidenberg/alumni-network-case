﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace AlumniNetworkBackend
{
    public class AlumniNetworkDbContext : DbContext
    {
        public DbSet<User> User { get; set; }
        public DbSet<Group> Group { get; set; }
        public DbSet<Message> Message { get; set; }

        public AlumniNetworkDbContext([NotNullAttribute] DbContextOptions options) : base(options)
        {

        }

        public AlumniNetworkDbContext()
        {

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(Environment.GetEnvironmentVariable("sqlConnectionString"));
        }
    }
}
