﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AlumniNetworkBackend
{
    public class MessageProfile : Profile
    {
        public MessageProfile()
        {
            // Message<->MessageReadDTO
            CreateMap<Message, MessageReadDTO>()
                .ForMember(mdto => mdto.ThreadMessages, opt => opt
                .MapFrom(m => m.ThreadMessages.Select(u => u.Id).ToArray()));

            // Message<->MessageCreateDTO
            CreateMap<Message, MessageCreateDTO>()
                .ReverseMap();
            // Message<->MessageEditDTO
            CreateMap<Message, MessageEditDTO>()
                .ReverseMap();
        }
    }
}
