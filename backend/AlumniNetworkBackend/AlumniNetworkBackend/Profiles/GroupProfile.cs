﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AlumniNetworkBackend
{
    public class GroupProfile : Profile
    {
        public GroupProfile()
        {
            // Group<->GroupReadDTO
            CreateMap<Group, GroupReadDTO>()
                .ForMember(gdto => gdto.Users, opt => opt
                .MapFrom(g => g.Users.Select(u => u.Id).ToArray()))
                .ForMember(gdto => gdto.Messages, opt => opt
                .MapFrom(g => g.Messages.Select(u => u.Id).ToArray()));


            // Group<->GroupCreateDTO
            CreateMap<Group, GroupCreateDTO>()
                .ReverseMap();
            // Group<->GroupEditDTO
            CreateMap<Group, GroupEditDTO>()
                .ReverseMap();
        }
    }
}
