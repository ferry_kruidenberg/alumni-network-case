﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AlumniNetworkBackend
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            // User<->UserReadDTO
            CreateMap<User, UserReadDTO>()
                .ForMember(udto => udto.Groups, opt => opt
                .MapFrom(u => u.Groups.Select(u => u.Id).ToArray()))
                .ForMember(udto => udto.ProfilePicture, opt => opt
                .MapFrom(u => "data:image/jpeg;base64," + Convert.ToBase64String(u.ProfilePicture)));

            // User<->UserCreateDTO
            CreateMap<User, UserCreateDTO>()
                .ReverseMap();
            // User<->UserEditDTO
            CreateMap<User, UserEditDTO>()
                .ReverseMap();
        }
    }
}
